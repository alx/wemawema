var domIsReady = (function(domIsReady) {
   var isBrowserIeOrNot = function() {
      return (!document.attachEvent || typeof document.attachEvent === "undefined" ? 'not-ie' : 'ie');
   }

   domIsReady = function(callback) {
      if(callback && typeof callback === 'function'){
         if(isBrowserIeOrNot() !== 'ie') {
            document.addEventListener("DOMContentLoaded", function() {
               return callback();
            });
         } else {
            document.attachEvent("onreadystatechange", function() {
               if(document.readyState === "complete") {
                  return callback();
               }
            });
         }
      } else {
         console.error('The callback is not a function!');
      }
   }

   return domIsReady;
})(domIsReady || {});

var svg           = new C2S(800,400);
var showSettings  = 'Show advanced settings';
var hideSettings  = 'Hide advanced settings';
var workCanvas    = 'Work with canvas';
var workSvg       = 'Work with SVG';
var customImgText = 'Custom image';
var customImg;

(function(document, window, domIsReady, undefined) {
    domIsReady(function() {
        // Try to get a translation
        var userLang = navigator.language || navigator.userLanguage;
        var xhr = new XMLHttpRequest();
        xhr.open('GET', 'locales/'+userLang+'.json');
        xhr.responseType = 'json';
        xhr.onload = function() {
            if (xhr.readyState == 4 && xhr.status === 200) {
                var locale = xhr.response;
                ['wemake', 'porn', 'filename', 'corners', 'wemake-color', 'porn-color', 'rect-color', 'add-glitter', 'bg-type', 'color', 'bgr-color', 'egr-color', 'bg-img', 'wemake-x', 'wemake-y', 'wemake-size', 'porn-x', 'porn-y', 'porn-size', 'custom-img', 'custom-x', 'custom-y', 'custom-size'].forEach(function(e) {
                    if (locale[e] !== undefined) {
                        document.querySelector('label[for="'+e+'"]').innerText = locale[e];
                    }
                });
                if (locale['corners-rounded'] !== undefined) {
                    document.querySelector('#corners option[value="20"]').innerText = locale['corners-rounded'];
                }
                if (locale['corners-square'] !== undefined) {
                    document.querySelector('#corners option[value="0"]').innerText = locale['corners-square'];
                }
                ['plain', 'gradient', 'rainbow', 'img', 'lr', 'rl', 'tb', 'bt', 'r', 'beer', 'glitter', 'rainbow-flag'].forEach(function(e) {
                    if (locale[e] !== undefined) {
                        document.querySelector('option[value="'+e+'"]').innerText = locale[e];
                    }
                });
                ['toggle-svg', 'download', 'reset', 'switch-colors'].forEach(function(e) {
                    if (locale[e] !== undefined) {
                        document.querySelector('#'+e).innerText = locale[e];
                        if (e === 'download') {
                            document.querySelector('#download-svg').innerText = locale[e];
                        }
                    }
                });
                if (document.getElementById('settings-block').classList.contains('hidden')) {
                    if (locale['settings-show'] !== undefined) {
                        document.querySelector('#settings').innerText = locale['settings-show'];
                    }
                } else if (locale['settings-hide'] !== undefined) {
                    document.querySelector('#settings').innerText = locale['settings-hide'];
                }
                if (locale['settings-show'] !== undefined) {
                    showSettings  = locale['settings-show'];
                }
                if (locale['settings-hide'] !== undefined) {
                    hideSettings  = locale['settings-hide'];
                }
                if (locale['toggle-canvas'] !== undefined) {
                    workCanvas    = locale['toggle-canvas'];
                }
                if (locale['toggle-svg'] !== undefined) {
                    workSvg       = locale['toggle-svg'];
                }
                if (locale['custom-img-text'] !== undefined) {
                    customImgText = locale['custom-img-text'];
                }
                if (locale['footer'] !== undefined) {
                    document.querySelector('footer p').innerHTML = locale['footer'];
                }
            }
        };
        xhr.send();

        // Trigger drawing
        document.querySelectorAll('input[type="text"]').forEach(function(e) {
            e.addEventListener('keyup', function() {
                refreshText();
                refreshFilename();
            });
            e.addEventListener('paste', function() {
                refreshText();
                refreshFilename();
            });
        });
        document.querySelectorAll('select,input[type="number"],input[type="color"],input[type="checkbox"]').forEach(function(e) {
            e.addEventListener('change', function() {
                refreshText();
            });
        });

        // Background type changes
        document.querySelector('#bg-type').addEventListener('change', function() {
            toggleBgSettings();
        });

        // Toggle SVG/Canvas
        document.querySelector('#toggle-svg').addEventListener('click', function() {
            var s = document.getElementById('svg');
            if (s.classList.contains('hidden')) {
                this.classList.add('button-primary');
                hide('canvas');
                hide('download');
                show('svg');
                show('download-svg');
                this.text = workCanvas;
            } else {
                this.classList.remove('button-primary');
                hide('svg');
                hide('download-svg');
                show('canvas');
                show('download');
                this.text = workSvg;
            }
            refreshFilename();
        });
        // Download
        document.querySelector('#download').addEventListener('click', function() {
            var f         = document.getElementById('filename');
            this.download = f.value;
            this.href     = document.getElementById('canvas').toDataURL().replace("image/png", "image/octet-stream");
        });
        document.querySelector('#download-svg').addEventListener('click', function() {
            var f         = document.getElementById('filename');
            this.download = f.value;
            this.href     = 'data:image/octet-stream;base64,'+btoa(svg.getSerializedSvg(true));
        });

        // Reset settings
        document.querySelector('#reset').addEventListener('click', function() {
            reset();
        });

        // Show settings
        document.querySelector('#settings').addEventListener('click', function() {
            var b = document.getElementById('settings-block');
            if (b.classList.contains('hidden')) {
                b.classList.remove('hidden');
                this.text = hideSettings;
            } else {
                b.classList.add('hidden');
                this.text = showSettings;
            }
        });

        // send to
        document.querySelector('#send-to-twitter').addEventListener('click', function() {
          var w   = document.getElementById('wemake');
          var wx  = document.getElementById('wemake-x');
          var wy  = document.getElementById('wemake-y');
          var ws  = document.getElementById('wemake-size');
          var wc  = document.getElementById('wemake-color');
          var p   = document.getElementById('porn');
          var px  = document.getElementById('porn-x');
          var py  = document.getElementById('porn-y');
          var ps  = document.getElementById('porn-size');
          var pc  = document.getElementById('porn-color');
          var co  = document.getElementById('color');
          var gli = document.getElementById('add-glitter');
          var bgt = document.getElementById('bg-type');
          var rc  = document.getElementById('rect-color');
          var bgr = document.getElementById('bgr-color');
          var egr = document.getElementById('egr-color');
          var or  = document.getElementById('gr-orientation');
          var bgi = document.getElementById('bg-img');
          var cor = document.getElementById('corners');

          var url = new URL(window.location);
          url.searchParams.set('w',   w.value);
          url.searchParams.set('wx',  wx.value);
          url.searchParams.set('wy',  wy.value);
          url.searchParams.set('ws',  ws.value);
          url.searchParams.set('wc',  wc.value);
          url.searchParams.set('p',   p.value);
          url.searchParams.set('px',  px.value);
          url.searchParams.set('py',  py.value);
          url.searchParams.set('ps',  ps.value);
          url.searchParams.set('pc',  pc.value);
          url.searchParams.set('co',  co.value);
          url.searchParams.set('gli', gli.checked);
          url.searchParams.set('bgt', bgt.value);
          url.searchParams.set('rc',  rc.value);
          url.searchParams.set('bgr', bgr.value);
          url.searchParams.set('egr', egr.value);
          url.searchParams.set('or',  or.value);
          url.searchParams.set('bgi', bgi.value);
          url.searchParams.set('cor', cor.value);
          url.searchParams.set('sendToTwitter', true);

          var http = new XMLHttpRequest();
          http.open("GET", url.toString(), true);
          http.onreadystatechange = function() {
            if(http.readyState == 4 && http.status == 200) {
              alert(http.responseText);
            }
          }
          http.send(null);
        });

        // Switch text colors
        document.querySelector('#switch-colors').addEventListener('click', function(e) {
            e.preventDefault();
            switchTextColors();
            refreshText();
        });

        // Upload custom image
        document.getElementById('custom-img').addEventListener('change', handleImage, false);
        if (customImg === undefined || customImg === null) {
            document.getElementById('custom-img').value = null;
        }

        // Get parameters
        if (window.location.hash) {
            document.querySelector('#porn').value = decodeURIComponent(window.location.hash.substr(1));
        }
        var url = new URL(window.location);
        if (url.searchParams.get('w') !== null) {
            document.getElementById('wemake').value         = url.searchParams.get('w');
        }
        if (url.searchParams.get('wx') !== null) {
            document.getElementById('wemake-x').value       = url.searchParams.get('wx');
        }
        if (url.searchParams.get('wy') !== null) {
            document.getElementById('wemake-y').value       = url.searchParams.get('wy');
        }
        if (url.searchParams.get('ws') !== null) {
            document.getElementById('wemake-size').value    = url.searchParams.get('ws');
        }
        if (url.searchParams.get('wc') !== null) {
            document.getElementById('wemake-color').value   = url.searchParams.get('wc');
        }
        if (url.searchParams.get('p') !== null) {
            document.getElementById('porn').value           = url.searchParams.get('p');
        }
        if (url.searchParams.get('px') !== null) {
            document.getElementById('porn-x').value         = url.searchParams.get('px');
        }
        if (url.searchParams.get('py') !== null) {
            document.getElementById('porn-y').value         = url.searchParams.get('py');
        }
        if (url.searchParams.get('ps') !== null) {
            document.getElementById('porn-size').value      = url.searchParams.get('ps');
        }
        if (url.searchParams.get('pc') !== null) {
            document.getElementById('porn-color').value     = url.searchParams.get('pc');
        }
        if (url.searchParams.get('co') !== null) {
            document.getElementById('color').value          = url.searchParams.get('co');
        }
        if (url.searchParams.get('gli') !== null) {
            document.getElementById('add-glitter').checked  = (url.searchParams.get('gli') === 'true') ? true : false;
        }
        if (url.searchParams.get('bgt') !== null) {
            document.getElementById('bg-type').value        = url.searchParams.get('bgt');
        }
        if (url.searchParams.get('rc') !== null) {
            document.getElementById('rect-color').value     = url.searchParams.get('rc');
        }
        if (url.searchParams.get('bgr') !== null) {
            document.getElementById('bgr-color').value      = url.searchParams.get('bgr');
        }
        if (url.searchParams.get('egr') !== null) {
            document.getElementById('egr-color').value      = url.searchParams.get('egr');
        }
        if (url.searchParams.get('or') !== null) {
            document.getElementById('gr-orientation').value = url.searchParams.get('or');
        }
        if (url.searchParams.get('bgi') !== null) {
            document.getElementById('bg-img').value         = url.searchParams.get('bgi');
        }
        if (url.searchParams.get('cor') !== null) {
            document.getElementById('corners').value        = url.searchParams.get('cor');
        }
        if (url.searchParams.get('f') !== null) {
            document.getElementById('filename').value       = url.searchParams.get('f');
        }

        toggleBgSettings();
        refreshFilename();
        refreshText();
    });
})(document, window, domIsReady);

function handleImage(e) {
    var reader    = new FileReader();
    reader.onload = function(event) {
        var img    = new Image();
        img.onload = function() {
            var o   = document.createElement('option');
            var b   = document.getElementById('bg-img');
            var c   = document.querySelector('option[value="custom"]');

            if (c !== null) {
                c.remove();
            }

            o.setAttribute('value', 'custom');
            o.text = customImgText;
            b.appendChild(o);
            b.value   = 'custom';
            customImg = img;

            document.getElementById('custom-x').value    = 0;
            document.getElementById('custom-y').value    = 0;
            document.getElementById('custom-size').value = 0;

            refreshText();
        }
        img.src = event.target.result;
    }
    reader.readAsDataURL(e.target.files[0]);
}

function scalePreserveAspectRatio(imgW,imgH,maxW,maxH){
    return(Math.min((maxW/imgW),(maxH/imgH)));
}

function switchTextColors() {
    var w  = document.getElementById('wemake-color');
    var p  = document.getElementById('porn-color');

    var c1 = w.value;
    var c2 = p.value;

    w.value = c2;
    p.value = c1;
}

function toggleBgSettings() {
    switch(document.getElementById('bg-type').value) {
        case 'plain':
            show('bg-color');
            hide('gradient-settings');
            hide('bg-img-settings');
            hide('bg-custom-img-settings');
            break;
        case 'gradient':
            hide('bg-color');
            show('gradient-settings');
            hide('bg-img-settings');
            hide('bg-custom-img-settings');
            break;
        case 'img':
            hide('bg-color');
            hide('gradient-settings');
            show('bg-img-settings');
            show('bg-custom-img-settings');
            break;
        default:
            hide('bg-color');
            hide('gradient-settings');
            hide('bg-img-settings');
            hide('bg-custom-img-settings');
    }
}

function show(id) {
    var b = document.getElementById(id);
    if (b.classList.contains('hidden')) {
        b.classList.remove('hidden');
    }
}

function hide(id) {
    var r = document.getElementById(id);
    if (!r.classList.contains('hidden')) {
        r.classList.add('hidden');
    }
}

function reset() {
    document.getElementById('wemake-x').value       = '400';
    document.getElementById('wemake-y').value       = '160';
    document.getElementById('wemake-size').value    = '150';
    document.getElementById('wemake-color').value   = '#000000';
    document.getElementById('porn-x').value         = '400';
    document.getElementById('porn-y').value         = '350';
    document.getElementById('porn-size').value      = '220';
    document.getElementById('porn-size').value      = '220';
    document.getElementById('porn-color').value     = '#000000';
    document.getElementById('color').value          = '#fcd205';
    document.getElementById('rect-color').value     = '#000000';
    document.getElementById('add-glitter').checked  = false;
    document.getElementById('bg-type').value        = 'plain';
    document.getElementById('gr-orientation').value = 'lr';
    document.getElementById('bg-img').value         = 'beer';
    document.getElementById('corners').value        = '20';
    document.getElementById('custom-img').value     = null;
    document.getElementById('custom-x').value       = 0;
    document.getElementById('custom-y').value       = 0;
    document.getElementById('custom-size').value    = 0;

    show('bg-color');
    hide('gradient-settings');
    hide('bg-img-settings');
    hide('bg-custom-img-settings');

    refreshText();
}

function refreshFilename() {
    var w = document.getElementById('wemake');
    var p = document.getElementById('porn');
    var f = document.getElementById('filename');
    var s = document.getElementById('svg');
    var ext = 'png';

    if (!s.classList.contains('hidden')) {
        ext = 'svg';
    }

    var name = w.value+' '+p.value+'.'+ext;
    f.value = name.replace(/ /g, '_');
}

function refreshText() {
    var w   = document.getElementById('wemake');
    var wx  = document.getElementById('wemake-x');
    var wy  = document.getElementById('wemake-y');
    var ws  = document.getElementById('wemake-size');
    var wc  = document.getElementById('wemake-color');
    var p   = document.getElementById('porn');
    var px  = document.getElementById('porn-x');
    var py  = document.getElementById('porn-y');
    var ps  = document.getElementById('porn-size');
    var pc  = document.getElementById('porn-color');
    var co  = document.getElementById('color');
    var gli = document.getElementById('add-glitter');
    var bgt = document.getElementById('bg-type');
    var rc  = document.getElementById('rect-color');
    var bgr = document.getElementById('bgr-color');
    var egr = document.getElementById('egr-color');
    var or  = document.getElementById('gr-orientation');
    var bgi = document.getElementById('bg-img');
    var cor = document.getElementById('corners');

    var c    = document.getElementById('canvas');

    var ctx  = c.getContext('2d');
        ctx.clearRect(0, 0, c.width, c.height);
        svg.clearRect(0, 0, c.width, c.height);

    // update URL
    var url = new URL(window.location);
    url.searchParams.set('w',   w.value);
    url.searchParams.set('wx',  wx.value);
    url.searchParams.set('wy',  wy.value);
    url.searchParams.set('ws',  ws.value);
    url.searchParams.set('wc',  wc.value);
    url.searchParams.set('p',   p.value);
    url.searchParams.set('px',  px.value);
    url.searchParams.set('py',  py.value);
    url.searchParams.set('ps',  ps.value);
    url.searchParams.set('pc',  pc.value);
    url.searchParams.set('co',  co.value);
    url.searchParams.set('gli', gli.checked);
    url.searchParams.set('bgt', bgt.value);
    url.searchParams.set('rc',  rc.value);
    url.searchParams.set('bgr', bgr.value);
    url.searchParams.set('egr', egr.value);
    url.searchParams.set('or',  or.value);
    url.searchParams.set('bgi', bgi.value);
    url.searchParams.set('cor', cor.value);
    window.history.pushState("change", "WemaWema", url);

    // background color
    switch(bgt.value) {
        case 'plain':
            ctx.fillStyle = co.value;
            svg.fillStyle = co.value;
            break;
        case 'gradient':
            var grd;
            switch(or.value) {
                case 'lr':
                    grd    = ctx.createLinearGradient(0, 200, c.width, 200);
                    grd2   = svg.createLinearGradient(0, 200, c.width, 200);
                    break;
                case 'rl':
                    grd    = ctx.createLinearGradient(c.width, 200, 0, 200);
                    grd2   = svg.createLinearGradient(c.width, 200, 0, 200);
                    break;
                case 'tb':
                    grd    = ctx.createLinearGradient(400, 0, 400, c.height);
                    grd2   = svg.createLinearGradient(400, 0, 400, c.height);
                    break;
                case 'bt':
                    grd    = ctx.createLinearGradient(400, c.height, 400, 0);
                    grd2   = svg.createLinearGradient(400, c.height, 400, 0);
                    break;
                case 'r':
                    grd    = ctx.createRadialGradient(400, 200, 25, 400, 200, 400);
                    grd2   = svg.createRadialGradient(400, 200, 25, 400, 200, 400);
                    break;
            }
            grd.addColorStop(0, bgr.value);
            grd.addColorStop(1, egr.value);
            grd2.addColorStop(0, bgr.value);
            grd2.addColorStop(1, egr.value);
            ctx.fillStyle = grd;
            svg.fillStyle = grd2;
            break;
        case 'img':
            if (bgi.value === 'custom' && customImg !== undefined && customImg !== null) {
                var iw = customImg.width;
                var ih = customImg.height;
                if (iw > 800 || ih > 400) {
                    var x = document.getElementById('custom-x').value;
                    var y = document.getElementById('custom-y').value;
                    var s = document.getElementById('custom-size');
                    var sizer = scalePreserveAspectRatio(iw, ih, 800, 400);
                    if (parseFloat(s.value) === 0.0) {
                        s.value = Math.round(sizer * 1000)/1000;
                    } else {
                        sizer = s.value;
                    }
                    ctx.drawImage(customImg, 0, 0, iw, ih, x, y, iw*sizer, ih*sizer);
                    svg.drawImage(customImg, 0, 0, iw, ih, x, y, iw*sizer, ih*sizer);
                } else {
                    ctx.drawImage(customImg, 0, 0);
                    svg.drawImage(customImg, 0, 0);
                }
                drawOver(ctx, svg, rc, ws, wc, w, wx, wy, ps, pc, p, px, py, gli);
            } else {
                if (bgi.value === '') {
                    bgi.value = 'beer';
                }
                var background = new Image();
                var img        = 'img/'+bgi.value
                if (parseInt(cor.value) === 20) {
                    img += '-round';
                }
                background.src = img+'.png';
                background.onload = function() {
                    ctx.drawImage(background, 0, 0);
                    svg.drawImage(background, 0, 0);
                    drawOver(ctx, svg, rc, ws, wc, w, wx, wy, ps, pc, p, px, py, gli);
                }
            }
            return;
            break;
        case 'rainbow':
            var grd  = ctx.createLinearGradient(400, 0, 400, c.height);
            var grd2 = svg.createLinearGradient(400, 0, 400, c.height);
            grd.addColorStop(0, '#e50000');
            grd.addColorStop(1 / 5, '#ff8d00');
            grd.addColorStop(2 / 5, '#ffee00');
            grd.addColorStop(3 / 5, '#008121');
            grd.addColorStop(4 / 5, '#004cff');
            grd.addColorStop(1, '#760188');
            grd2.addColorStop(0, '#e50000');
            grd2.addColorStop(1 / 5, '#ff8d00');
            grd2.addColorStop(2 / 5, '#ffee00');
            grd2.addColorStop(3 / 5, '#008121');
            grd2.addColorStop(4 / 5, '#004cff');
            grd2.addColorStop(1, '#760188');
            ctx.fillStyle = grd;
            svg.fillStyle = grd2;
            break;
    }
    roundRect(ctx, 0, 0, c.width, c.height, parseInt(cor.value), true, false);
    roundRect(svg, 0, 0, c.width, c.height, parseInt(cor.value), true, false);

    drawOver(ctx, svg, rc, ws, wc, w, wx, wy, ps, pc, p, px, py, gli);
}

function drawOver(ctx, svg, rc, ws, wc, w, wx, wy, ps, pc, p, px, py, gli) {
    // Inside rounded rectangle
    ctx.lineWidth   = 20;
    svg.lineWidth   = 20;
    ctx.strokeStyle = rc.value;
    svg.strokeStyle = rc.value;
    roundRect(ctx, 25, 25, 750, 350, 20, false);
    roundRect(svg, 25, 25, 750, 350, 20, false);

    ctx.textAlign="center";
    svg.textAlign="center";
    // Write WE MAKE
    ctx.font      = 'bold '+parseInt(ws.value)+'px sans-serif';
    svg.font      = 'bold '+parseInt(ws.value)+'px sans-serif';
    ctx.fillStyle = wc.value;
    svg.fillStyle = wc.value;
    ctx.fillText(w.value, parseInt(wx.value), parseInt(wy.value), 725);
    svg.fillText(w.value, parseInt(wx.value), parseInt(wy.value), 725);

    // Write PORN
    ctx.font      = 'bold '+parseInt(ps.value)+'px sans-serif';
    svg.font      = 'bold '+parseInt(ps.value)+'px sans-serif';
    ctx.fillStyle = pc.value
    svg.fillStyle = pc.value
    ctx.fillText(p.value, parseInt(px.value), parseInt(py.value), 725);
    svg.fillText(p.value, parseInt(px.value), parseInt(py.value), 725);

    if (gli.checked) {
        var glitter = new Image();
        glitter.src = 'img/glitter-calc.png';
        glitter.onload = function() {
            ctx.drawImage(glitter, 0, 0);
            svg.drawImage(glitter, 0, 0);
            document.getElementById('svg').innerHTML = svg.getSerializedSvg();
        }
    } else {
        document.getElementById('svg').innerHTML = svg.getSerializedSvg();
    }
}

function roundRect(ctx, x, y, width, height, radius, fill, stroke) {
    if (typeof stroke == 'undefined') {
        stroke = true;
    }
    if (typeof radius === 'undefined') {
        radius = 5;
    }
    if (typeof radius === 'number') {
        radius = {tl: radius, tr: radius, br: radius, bl: radius};
    } else {
        var defaultRadius = {tl: 0, tr: 0, br: 0, bl: 0};
        for (var side in defaultRadius) {
            radius[side] = radius[side] || defaultRadius[side];
        }
    }
    ctx.beginPath();
    ctx.moveTo(x + radius.tl, y);
    ctx.lineTo(x + width - radius.tr, y);
    ctx.quadraticCurveTo(x + width, y, x + width, y + radius.tr);
    ctx.lineTo(x + width, y + height - radius.br);
    ctx.quadraticCurveTo(x + width, y + height, x + width - radius.br, y + height);
    ctx.lineTo(x + radius.bl, y + height);
    ctx.quadraticCurveTo(x, y + height, x, y + height - radius.bl);
    ctx.lineTo(x, y + radius.tl);
    ctx.quadraticCurveTo(x, y, x + radius.tl, y);
    ctx.closePath();
    if (fill) {
        ctx.fill();
    }
    if (stroke) {
        ctx.stroke();
    }
}
